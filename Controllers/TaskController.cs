﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TaskApi.ITaskRepository;
using TaskApi.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TaskApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TaskController : ControllerBase
    {
        ITask _repo;
        public TaskController(ITask repo)
        {
            _repo = repo;
        }
        [HttpGet]
        public ActionResult<List<Tasks>> GetTasks()
        {
            if (_repo.GetTasks().ToList().Count == 0)
            {
                return NotFound("There are no Records");
            }
            else
            {
                return _repo.GetTasks();
            }
        }
        [HttpGet("{id}")]
        public ActionResult<int> GetTasksById(int id)
        {
            if (_repo.GetTaskById(id) == null)
                return 0;
            else
                return Ok(_repo.GetTaskById(id));
        }
        [HttpPost]
        public ActionResult<int> CreateTask(Tasks task)
        {
            _repo.Create(task);
            return Created("Created", task);
        }
        [HttpPut("{id}")]
        public void EditTask(int id, Tasks task)
        {
            _repo.Edit(id, task);
        }
        [HttpDelete("{id}")]
        public ActionResult<string> DeleteStudent(int id)
        {
            Tasks task = _repo.GetTaskById(id);
            if (task == null)
                return "There is no Record";
            else
            {
                _repo.Delete(id);
                return Ok("Record Deleted");
            }
        }

    }
}
