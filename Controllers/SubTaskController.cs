﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TaskApi.ITaskRepository;
using TaskApi.Models;

namespace TaskApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SubTaskController : ControllerBase
    {

        // Add required dependencies here
        //public async Task<IActionResult> GetAllSubtasks()
        //{

        //}

        ISubTask _repo;
        public SubTaskController(ISubTask repo)
        {
            _repo = repo;
        }
        [HttpGet]
        public ActionResult<List<SubTask>> GetSubTasks()
        {
            if(_repo.GetSubTasks().ToList().Count() == 0)
            {
                return NotFound("There is no record:");
            }
            else
            {
                return _repo.GetSubTasks();
            }
        }
        [HttpGet("{id}")]
        public ActionResult<int> GetSubTasksById(int id)
        {
            if(_repo.GetSubTaskById(id) == null)
            {
                return 0;
            }
            else
            {
                return Ok(_repo.GetSubTaskById(id));
            }
        }
        [HttpPost]
        public ActionResult<SubTask> Create(SubTask subTask)
        {
            _repo.Create(subTask);
            return Created("Created", subTask);
        }
    }
}
