﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TaskApi.Migrations
{
    /// <inheritdoc />
    public partial class updated : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Tasks",
                newName: "TasksId");

            migrationBuilder.AddColumn<int>(
                name: "TaskId",
                table: "SubTasks",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TasksId",
                table: "SubTasks",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SubTasks_TasksId",
                table: "SubTasks",
                column: "TasksId");

            migrationBuilder.AddForeignKey(
                name: "FK_SubTasks_Tasks_TasksId",
                table: "SubTasks",
                column: "TasksId",
                principalTable: "Tasks",
                principalColumn: "TasksId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SubTasks_Tasks_TasksId",
                table: "SubTasks");

            migrationBuilder.DropIndex(
                name: "IX_SubTasks_TasksId",
                table: "SubTasks");

            migrationBuilder.DropColumn(
                name: "TaskId",
                table: "SubTasks");

            migrationBuilder.DropColumn(
                name: "TasksId",
                table: "SubTasks");

            migrationBuilder.RenameColumn(
                name: "TasksId",
                table: "Tasks",
                newName: "Id");
        }
    }
}
