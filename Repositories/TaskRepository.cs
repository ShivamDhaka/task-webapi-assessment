﻿
using Microsoft.EntityFrameworkCore;
using TaskApi.Context;
using TaskApi.ITaskRepository;
using TaskApi.Models;

namespace TaskApi.Repositories
{
    public class TaskRepository : ITask
    {
        TaskDbContext _db;
        public TaskRepository(TaskDbContext db)
        {
            _db = db;
        }
        public Tasks Create(Tasks task)
        {
            _db.Tasks.Add(task);
            _db.SaveChanges();
            return task;
        }

        public int Delete(int id)
        {
            var obj = GetTaskById(id);
            if (obj != null)
            {
                _db.Tasks.Remove(obj);
                _db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Edit(int id, Tasks task)
        {
            var obj = GetTaskById(id);
            if (obj != null)
            {
                foreach (Tasks temp in _db.Tasks)
                {
                    if (temp.Id == id)
                    {
                        temp.Name = task.Name;
                        temp.Description = task.Description;
                        temp.CreatedBy = task.CreatedBy;
                    }
                }
                _db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public Tasks GetTaskById(int id)
        {
            return _db.Tasks.FirstOrDefault(x => x.Id == id);
        }

        public List<Tasks> GetTasks()
        {
            return _db.Tasks.ToList();
        }
    }
}