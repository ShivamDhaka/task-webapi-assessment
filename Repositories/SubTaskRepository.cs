﻿using Microsoft.EntityFrameworkCore;
using TaskApi.Context;
using TaskApi.ITaskRepository;
using TaskApi.Models;

namespace TaskApi.Repositories
{
    public class SubTaskRepository : ISubTask
    {
        TaskDbContext _db;
        public SubTaskRepository(TaskDbContext db)
        {
            _db = db;
        }

        public SubTask Create(SubTask task)
        {
            _db.SubTasks.Add(task);
            _db.SaveChanges();
            return task;
        }

        public List<SubTaskViewModel> GetSubTaskById(int id)
        {
            var list = (from x in _db.Tasks
                        join y in _db.SubTasks
                        on x.Id equals y.TasksId
                        select new SubTaskViewModel
                        {
                            Id = x.Id,
                            TaskName = x.Name,
                            SubTaskName = y.Name,
                            SubTaskCreatedBy = y.CreatedBy,
                            SubTaskCreatedOn = DateTime.Now,

                        }
                       ).ToList();
            return list;

        }

        public List<SubTask> GetSubTasks()
        {
            return _db.SubTasks.ToList();
        }

        List<SubTaskViewModel> ISubTask.GetSubTaskById(int id)
        {
            throw new NotImplementedException();
        }
    }
}
