﻿namespace TaskApi.Models
{
    public class SubTask
    {// Add properties
        // Id, SubTaskName, Created By, Created On, Description,
        // TaskId
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set;}
        public int TasksId { get; set; }
        public Tasks? tasks { get; set; }
    }
}
