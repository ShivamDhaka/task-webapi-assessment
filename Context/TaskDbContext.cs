﻿using Microsoft.EntityFrameworkCore;
using TaskApi.Models;

namespace TaskApi.Context
{
    public class TaskDbContext : DbContext
    {
        public TaskDbContext()
        {
            
        }
        public TaskDbContext(DbContextOptions<TaskDbContext> options) :base(options)
        {
            
        }
        public DbSet<Tasks> Tasks { get; set;}
        public DbSet<SubTask> SubTasks { get; set;}
        public DbSet<User> Users { get; set;}
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
            .HasData(new User
            {
                UserId = 1,
                UserName = "user1",
                Password = "user1"
            },
            new User
            {
                UserId = 2,
                UserName = "user2",
                Password = "user2"
            },
            new User
            {
                UserId = 3,
                UserName = "user3",
                Password = "user3"
            }
            );
        }

    }
}
