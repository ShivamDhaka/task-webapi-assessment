﻿using TaskApi.Models;

namespace TaskApi.ITaskRepository
{
    public interface ISubTask
    {
        List<SubTask> GetSubTasks();
        SubTask Create(SubTask subtask);
        List<SubTaskViewModel> GetSubTaskById(int id);
    }
}
